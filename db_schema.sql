CREATE TABLE contact (
    contactId int AUTO_INCREMENT PRIMARY KEY,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL
);

CREATE TABLE phoneNumber (
    phoneNumberId int AUTO_INCREMENT PRIMARY KEY,
    pNumber VARCHAR(255) NOT NULL,
    contactId int,
    FOREIGN KEY(contactId) REFERENCES contact(contactId)
);