<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

include_once("models/Contact.php");
require_once 'Request.php';
require_once 'vendor/autoload.php';
require_once 'controllers/ContactController.php';

$loader = new FilesystemLoader('views');
$twig = new Environment($loader, ['debug' => true]);
$contactController = new ContactController();

$request = new Request($_REQUEST);
$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'list-page';

switch ($cmd) {
    case 'add':
        echo $twig->render('add.html.twig', [
            'title' => 'Lisa uus kasutaja'
        ]);
        break;
    case 'save-user':
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];
        $phoneNumber1 = $_POST["phone1"];
        $phoneNumber2 = $_POST["phone2"];
        $phoneNumber3 = $_POST["phone3"];
        $existingContact = $_POST["contactId"];

        if (strlen($existingContact) > 0 && strlen($firstName) >= 2 && strlen($firstName) < 50 &&
            strlen($lastName) >= 2 && strlen($lastName) < 50) {
            header('Location: ' . '?cmd=list-page&message=updated');
            $contactController->updateContact($existingContact, $firstName, $lastName);
            $contactController->updateContactPhones(
                $_POST['phoneId1'], $phoneNumber1, $existingContact);
            $contactController->updateContactPhones(
                $_POST['phoneId2'], $phoneNumber2, $existingContact);
            $contactController->updateContactPhones(
                $_POST['phoneId3'], $phoneNumber3, $existingContact);

        } else if (strlen($firstName) >= 2 && strlen($firstName) < 50 &&
            strlen($lastName) >= 2 && strlen($lastName) < 50) {
            header('Location: ' . '?cmd=list-page&message=success');
            $contactController->addNewContact($firstName, $lastName,
                $phoneNumber1, $phoneNumber2, $phoneNumber3);

        } else {
            $contact = new Contact(1, $firstName, $lastName);
            $contactController->cacheUserPhoneNumber($phoneNumber1, $contact);
            $contactController->cacheUserPhoneNumber($phoneNumber2, $contact);
            $contactController->cacheUserPhoneNumber($phoneNumber3, $contact);
            echo $twig->render('add.html.twig', [
                'title' => 'Lisa uus kasutaja',
                'error' => true,
                'contact' => $contact,
            ]);
        }
        break;
    case 'edit-contact':
        $existingContact = $_GET['person_id'];
        $contact = $contactController->getSingleContact($existingContact);
        echo $twig->render('add.html.twig', [
            'title' => 'Muuda kontakti',
            'contact' => $contact,
            'id' => $existingContact,
            'edit' => true
        ]);
        break;
    default:
        echo $twig->render('list.html.twig',
            [
                'title' => 'List kontaktidest',
                'contacts' => $contactController->getContacts(),
                'message' => $_GET['message']
            ]);
        break;
}

