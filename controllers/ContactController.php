<?php

include_once("models/Contact.php");
include_once("models/PhoneNumber.php");

class ContactController {
    const GET_USERS_AND_PHONES = 'select c.contactId, c.firstName, 
       c.lastName, p.pNumber, p.phoneNumberId as phoneId, p.contactId as contactId from tristankrass.contact 
        c LEFT JOIN tristankrass.phoneNumber p on c.contactId = p.contactId;';
    const USERNAME = "tristankrass";
    const PASSWORD = "faf3";
    private $dbConn = null;

    /**
     * ContactController constructor.
     */
    public function __construct() {
        try {
            $this->dbConn = new PDO("mysql:host=db;dbname=tristankrass",
                self::USERNAME, self::PASSWORD);
            $this->dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new PDOException($e);
        }
    }


    public function addNewContact($firstName, $lastName = "",
                                  $phoneNumber1 = "", $phoneNumber2 = "",
                                  $phoneNumber3 = "") {
        $statement = 'insert into tristankrass.contact (firstName, lastName) values 
                                                               (:firstName, :lastName)';
        $this->sqlStatementHandler($statement, [':firstName' => $firstName,
            ':lastName' => $lastName]);
        $id = $this->dbConn->lastInsertId();
        $this->addPhoneNumber($phoneNumber1, $id);
        $this->addPhoneNumber($phoneNumber2, $id);
        $this->addPhoneNumber($phoneNumber3, $id);
    }

    public function addPhoneNumber($phoneNumber, $contactId) {
        if (strlen($phoneNumber) <= 0) {
            return;
        }
        $statement = 'INSERT into tristankrass.phoneNumber (pNumber, contactId) VALUES 
                                                            (:pNumber, :contactId)';
        $this->sqlStatementHandler($statement, [':pNumber' => $phoneNumber,
            ':contactId' => $contactId]);
    }

    public function getContacts() {
        $stmt = $this->sqlStatementHandler(self::GET_USERS_AND_PHONES, []);
        $users = [];
        foreach ($stmt as $user) {
            $id = $user["contactId"];
            if (isset($users[$id])) {
                $userInstance = $users[$id];
                $phoneNum = $user['pNumber'];
                $userInstance->setPhoneNumber(
                    new PhoneNumber($user['phoneId'],
                        $user['contactId'], $phoneNum));
            } else {
                $userInstance = new Contact($id, $user['firstName'], $user['lastName']);
                $users[$id] = $userInstance;
                $userInstance->setPhoneNumber(
                    new PhoneNumber($user['phoneId'], $id, $user['pNumber']));
            }
        }
        return $users;
    }

    public function getSingleContactPhoneNumbers($id, Contact $contact) {
        $stmt = $this->dbConn->prepare(
            'select contactId,pNumber, contactId from tristankrass.phoneNumber where contactId = :id;');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        foreach ($stmt as $phoneNumber) {
            $contact->setPhoneNumber(new PhoneNumber($phoneNumber['id'],
                $phoneNumber['contactId'], $phoneNumber['pNumber']));
        }
        return $contact;

    }

    public function getSingleContact(string $id) {
        $statement = 'SELECT c.contactId, c.firstName, c.lastName from 
                                          tristankrass.contact c WHERE c.contactId = :id;';
        $stmt = $this->sqlStatementHandler($statement, [':id' => $id]);
        foreach ($stmt as $user) {
            $userInstance = new Contact($id, $user['firstName'], $user['lastName']);
            return $this->getSingleContactPhoneNumbers($id, $userInstance);
        }
    }

    public function updateContact($id, $firstName, $lastName) {
        $statement = 'UPDATE tristankrass.contact SET firstName = :firstname,
                                 lastName = :lastname WHERE contactId = :id;';
        $this->sqlStatementHandler($statement, [':id' => $id, ':firstname' => $firstName,
            ':lastname' => $lastName]);
    }

    public function updateContactPhones($phoneId, $phoneNumber, $contactId) {
        if (strlen($phoneId == 0)) {
            $this->addPhoneNumber($phoneNumber, $contactId);
        }
        $statement = 'UPDATE tristankrass.phoneNumber SET 
                               number = :phoneNumber WHERE phoneNumberId = :phoneId;';
        $this->sqlStatementHandler($statement, [':phoneId' => $phoneId,
            ':phoneNumber' => $phoneNumber]);
    }

    public static function cacheUserPhoneNumber($phoneNumber, $user) {
        if (strlen($phoneNumber) > 0) {
            $user->setPhoneNumber(new PhoneNumber(3, 3, $phoneNumber));
        }
    }

    private function sqlStatementHandler($statement, $options) {
        try {
            $stmt = $this->dbConn->prepare($statement);
            foreach ($options as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->execute();
            return $stmt;
        } catch (PDOException $exception) {
            throw new PDOException($exception);
        }
    }
}
