document.addEventListener("DOMContentLoaded", () => {
  function hideElement(identificator) {
    document.querySelector(identificator).style.display = "none";
  }

  setTimeout(function() {
    hideElement("#content > div");
  }, 2000);

  setTimeout(function() {
    hideElement("#error-block");
  }, 5000);

  document
    .querySelector("#closeErrorMsg")
    .addEventListener("click", function() {
      hideElement("#error-block");
    });
});
