<?php

class Contact {
    private $firstName;
    public $id;
    private $lastName;
    public $phoneNumbers;

    /**
     * Contact constructor.
     * @param $id
     * @param string $firstName
     * @param string $lastName
     * @param string $phone
     */
    public function __construct($id, $firstName = "", $lastName = "", $phone = "") {
        $this->firstName = $firstName;
        $this->id = $id;
        $this->lastName = $lastName;
        if (strlen($phone) > 0) {
            array_push($this->phoneNumbers, $phone);
        }
        $this->phoneNumbers = [];
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumbers() {
        if (!empty($this->phoneNumbers)) {
            return implode(", ", $this->phoneNumbers);
        }
        return "";
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber(PhoneNumber $phoneNumber) {
        $this->phoneNumbers[] = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    public function __toString() {
        return $this->firstName . " " . $this->lastName;
    }
}