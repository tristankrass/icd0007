<?php


class PhoneNumber {
    private $phoneId;
    private $contactId;
    private $phoneNumber;

    /**
     * PhoneNumber constructor.
     * @param $phoneId
     * @param $contactId
     * @param $phoneNumber
     */
    public function __construct($phoneId, $contactId, $phoneNumber) {
        $this->phoneId = $phoneId;
        $this->contactId = $contactId;
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getPhoneId() {
        return $this->phoneId;
    }

    /**
     * @return mixed
     */
    public function getContactId() {
        return $this->contactId;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function __toString() {
        return $this->phoneNumber;
    }


}