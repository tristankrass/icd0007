# Icd0007 ehk Veebitehnoloogiad

## Running the app
```
docker-compose up --build
```
It will start the following: 
  1. The php container where the app will be running. on port 8000
  2. The db container where the mysql will be running on port 3306
  3. Adminer container (a web interface for browsing the db schema) on port 8080
  4. PhpMyAdmin container (a web interface for browsing the db schema) on port 5000
You can comment out adminer and/or phpmyadmin from the `docker-compose.yml` file since they are not nessecary to run the app

When first running the app, make sure to run the `db_schema.sql` file inside `tristankrass` database since the database is not populated when first booted.

**NB!**

Also added a login and logout functionality on the branch *login-logout*. This functionality is achieved by using cookies. More specifically I used PHP built in function `session_start()` and `session_end()` to generate cookies automatically
Additional info can be found from [php.net](https://www.php.net/manual/en/function.session-start.php)

### Erd Model
![Erd image](erd.png)